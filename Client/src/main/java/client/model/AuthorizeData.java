package client.model;

import java.util.Random;

/**
 * Created by grigory on 12/13/16.
 */
public class AuthorizeData {
    private String clientId;
    private String passwordHash;
    private String accessToken;
    private String refreshToken;
    private String csrfCode;

    public AuthorizeData(String clientId, String passwordHash) {
        this.clientId = clientId;
        this.passwordHash = passwordHash;
        this.accessToken = "";
        this.refreshToken = "";
        this.csrfCode = generateHeximalString(16);
    }

    public AuthorizeData(String clientId, String passwordHash, String accessToken, String refreshToken, String csrfCode) {
        this.clientId = clientId;
        this.passwordHash = passwordHash;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.csrfCode = csrfCode;
    }

    public String getClientId() {
        return clientId;
    }

    public AuthorizeData setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public AuthorizeData setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public AuthorizeData setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public AuthorizeData setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public String getCsrfCode() {
        return csrfCode;
    }

    public AuthorizeData setCsrfCode(String csrfCode) {
        this.csrfCode = csrfCode;
        return this;
    }

    public String generateHeximalString(int hexLength) {
        Random randomService = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hexLength; i++) {
            sb.append(Integer.toHexString(randomService.nextInt()));
        }
        sb.setLength(hexLength);
        return sb.toString();
    }

}
