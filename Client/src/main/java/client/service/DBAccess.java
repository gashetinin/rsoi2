package client.service;

import client.web.model.Requests.ItemRequest;
import client.web.model.Response.ItemResponse;
import client.web.model.Response.ListOfItemResponces;

/**
 * Created by grigory on 12/11/16.
 */
public interface DBAccess {

    public String addItem (ItemRequest itemRequest, String accessToken);

    public ItemResponse getItem (Long itemId);

    public String deleteItem (Long itemId, String accessToken);

    public String updateItem(Long itemId, ItemRequest itemRequest, String accessToken);

    public ListOfItemResponces getAllItems();

    public ListOfItemResponces getItems(int startIndex, int finishIndex);

}
