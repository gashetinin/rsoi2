package client.service;

import client.web.model.Requests.ItemRequest;
import client.web.model.Response.ItemResponse;
import client.web.model.Response.ListOfItemResponces;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * Created by grigory on 12/11/16.
 */
@Service
public class DBAccessImpl implements DBAccess {

    @Override
    public String addItem (ItemRequest itemRequest, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        HttpEntity<ItemRequest> requestUpdate = new HttpEntity<ItemRequest>(itemRequest,headers);
        ResponseEntity<Long> response = restTemplate.exchange("http://localhost:8081/item", HttpMethod.POST, requestUpdate, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        Long itemId = response.getBody();
        return "Item added: id="+itemId.toString();
    }

    @Override
    public ItemResponse getItem(Long itemId) {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:8081/item/"+ Long.toString(itemId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ItemResponse itemResponse = new ItemResponse(restTemplate.getForObject(uri, ItemResponse.class));
        return itemResponse;
    }

    @Override
    public String deleteItem(Long itemId, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        String address = "http://localhost:8081/item/"+ Long.toString(itemId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        HttpEntity<ItemRequest> requestDelete = new HttpEntity<ItemRequest>(null,headers);
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.DELETE, requestDelete, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        return "Deleted";
    }

    @Override
    public String updateItem(Long itemId, ItemRequest itemRequest, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        String address = "http://localhost:8081/item/"+ Long.toString(itemId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        HttpEntity<ItemRequest> requestUpdate = new HttpEntity<ItemRequest>(itemRequest,headers);
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        itemId = response.getBody();
        return "Item updated: id="+itemId.toString();
    }

    @Override
    public ListOfItemResponces getAllItems() {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:8081/item/";
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfItemResponces> response = restTemplate.getForEntity(uri, ListOfItemResponces.class);
        return response.getBody();
    }

    @Override
    public ListOfItemResponces getItems(int startIndex, int finishIndex) {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:8081/item/" + startIndex + "/" + finishIndex;
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfItemResponces> response = restTemplate.getForEntity(uri, ListOfItemResponces.class);
        return response.getBody();
    }
}
