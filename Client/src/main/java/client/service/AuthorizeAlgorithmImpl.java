package client.service;

import client.model.AuthorizeData;
import client.model.OAuth2Token;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * Created by grigory on 12/13/16.
 */
@Service
public class AuthorizeAlgorithmImpl implements AuthorizeAlgorithm {

    private AuthorizeData authorizeData = new AuthorizeData("gashetinin", "playmobil");
    private final String authorizeUrl = "http://localhost:8081/authorize";
    private final String backUrl = "http://localhost:8082/authorize/authorizationCode";
    private final String accessTokenUrl = "http://localhost:8081/authorize/accessToken";
    private final String refreshUrl = "http://localhost:8081/authorize/refreshToken";

    @Override
    public String getAuthorizeUrl() {
        String authorizeUrlFull = authorizeUrl + "?clientId="+authorizeData.getClientId() +
                "&response_type=code&callbackUrl="+backUrl + "&csrf="+authorizeData.getCsrfCode();
        return authorizeUrlFull;
    }

    @Override
    public boolean requestAccessToken(String authorizationCode) {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(accessTokenUrl).build().toUri();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization","authorizationCode="+authorizationCode +"&response_type=bearer&csrf="+authorizeData.getCsrfCode());

        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<OAuth2Token> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, OAuth2Token.class);
        OAuth2Token oAuth2Token = response.getBody();

        boolean Flag = new String(oAuth2Token.getCsrf()).equals(authorizeData.getCsrfCode());

        if (Flag) {
            authorizeData.setAccessToken(oAuth2Token.getToken());
            authorizeData.setRefreshToken(oAuth2Token.getRefreshToken());
            return true;
        }
        return false;
    }

    @Override
    public boolean refreshToken() {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(refreshUrl).build().toUri();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization","refreshToken="+authorizeData.getRefreshToken() +"&response_type=refresh_token&csrf="+authorizeData.getCsrfCode());

        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<OAuth2Token> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, OAuth2Token.class);
        OAuth2Token oAuth2Token = response.getBody();

        boolean Flag = new String(oAuth2Token.getCsrf()).equals(authorizeData.getCsrfCode());

        if (Flag) {
            authorizeData.setAccessToken(oAuth2Token.getToken());
            authorizeData.setRefreshToken(oAuth2Token.getRefreshToken());
            return true;
        }
        return false;
    }

    @Override
    public String getAccessToken() {
        return authorizeData.getAccessToken();
    }

    @Override
    public String getCsrf() {
        return  authorizeData.getCsrfCode();
    }


}
