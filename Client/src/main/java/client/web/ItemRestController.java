package client.web;

import client.service.AuthorizeAlgorithm;
import client.service.DBAccess;
import client.web.model.Requests.ItemRequest;
import client.web.model.Requests.ItemUpdateRequest;
import client.web.model.Requests.PageParams;
import client.web.model.Response.ItemResponse;
import client.web.model.Response.ListOfItemResponces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by grigory on 11/30/16.
 */
@RestController
@RequestMapping("/item")
public class ItemRestController {
    @Autowired
    private DBAccess dbAccess;

    @Autowired
    private AuthorizeAlgorithm authorizeAlgorithm;

    @RequestMapping(value = "/addItem", method = RequestMethod.GET)
    public ModelAndView addItemPage() {
        return new ModelAndView("addItemPage");
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/addItem", method = RequestMethod.POST)
    public String createItem(ItemRequest itemRequest) {
        String result = dbAccess.addItem(itemRequest, authorizeAlgorithm.getAccessToken());
        return result;
    }

    @RequestMapping(value = "/getItem", method = RequestMethod.GET)
    public ModelAndView getItemPage() {
        return new ModelAndView("getItemPage");
    }

    @RequestMapping(value="/getItem", method = RequestMethod.POST)
    public ItemResponse getItem(Integer id) {
        return dbAccess.getItem(Integer.toUnsignedLong(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ItemResponse> getAll() {
        ListOfItemResponces listOfItemResponces= dbAccess.getAllItems();
        if (listOfItemResponces != null)
            return listOfItemResponces.getItemResponses();
        return null;
    }

    @RequestMapping(value="/getPage", method = RequestMethod.GET)
    public ModelAndView getAllPagination() {
        return new ModelAndView("getItemPaginationPage");
    }

    @RequestMapping(value="/getPage", method = RequestMethod.POST)
    public List<ItemResponse> getAllPagination(PageParams pageParams) {
        ListOfItemResponces listOfItemResponces= dbAccess.getItems(pageParams.getStartIndex(),pageParams.getFinishIndex());
        if (listOfItemResponces != null)
            return listOfItemResponces.getItemResponses();
        return null;
    }

    @RequestMapping(value = "/updateItem", method = RequestMethod.GET)
    public ModelAndView updateItem() {
        return new ModelAndView("updateItemPage");
    }

    @RequestMapping(value = "/updateItem", method = RequestMethod.POST)
    public String updateItem(ItemUpdateRequest itemUpdateRequest) {
        return dbAccess.updateItem(itemUpdateRequest.getItemId(), new ItemRequest(itemUpdateRequest.getName(),itemUpdateRequest.getPrice()), authorizeAlgorithm.getAccessToken());
    }

    @RequestMapping(value = "/deleteItem", method = RequestMethod.GET)
    public ModelAndView deleteItem() {
        return new ModelAndView("deleteItemPage");
    }

    @RequestMapping(value = "/deleteItem", method = RequestMethod.POST)
    public String deleteItem(Integer id) {
        dbAccess.deleteItem(Integer.toUnsignedLong(id), authorizeAlgorithm.getAccessToken());
        return "Item deleted!";
    }

}
