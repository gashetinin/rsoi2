package client.web.model.Requests;

import com.google.common.base.MoreObjects;

import java.util.List;

/**
 * Created by grigory on 11/28/16.
 */
public class OrderRequest {
    private Integer totalPrice;
    private List<ItemRequest> items;

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public OrderRequest setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public List<ItemRequest> getItems() {
        return items;
    }

    public OrderRequest setItems(List<ItemRequest> items) {
        this.items = items;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .toString();
    }
}
