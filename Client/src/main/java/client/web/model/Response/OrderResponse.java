package client.web.model.Response;

import com.google.common.base.MoreObjects;

import java.util.List;

/**
 * Created by grigory on 11/30/16.
 */
public class OrderResponse {
    private Integer totalPrice;
    private List<ItemResponse> items;
    private BuffetResponse buffet;
    private UserResponse user;

    public OrderResponse(OrderResponse order) {
        this.totalPrice = order.getTotalPrice();
        if (!order.getItems().isEmpty())
            for (ItemResponse item: order.getItems()) {
                items.add(new ItemResponse(item));
            }
        if (order.getBuffet() != null)
            this.buffet = new BuffetResponse(order.getBuffet());
        if (order.getUser() != null)
            this.user = new UserResponse(order.getUser());
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public List<ItemResponse> getItems() {
        return items;
    }

    public BuffetResponse getBuffet() {
        return buffet;
    }

    public UserResponse getUser() {
        return user;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .add("buffet", buffet)
                .add("user", user)
                .toString();
    }
}
