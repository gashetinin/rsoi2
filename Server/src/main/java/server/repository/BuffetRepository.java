package server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import server.domain.Buffet;

/**
 * Created by grigory on 11/27/16.
 */
public interface BuffetRepository extends JpaRepository<Buffet,Long> {
}