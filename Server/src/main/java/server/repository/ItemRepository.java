package server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import server.domain.Item;

/**
 * Created by grigory on 11/27/16.
 */
public interface ItemRepository extends JpaRepository<Item,Long> {
}